import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ExcelImportService } from './shared/services/excelimport.service';
const xlsxFile = require('read-excel-file/node');

async function bootstrap() {
  const application = await NestFactory.createApplicationContext(
    AppModule,
  );

  const command = process.argv[2];

  switch (command) {
    case 'import-excel-data':
      const excelImportService = application.get(ExcelImportService);
      //account data
      await xlsxFile('./OCIO.xlsx').then(async (rows) => {
        rows.shift();
        let  accountsData = [];
        rows.forEach(async (row) => {
          //console.log(row);
          let account = {
            AccountID: row[0],
            AccountSize: row[1],
            Period: row[2],
            AccountStatusDesc: row[3],
            ClientTypeLabel: row[4],
            PlanTypeLabel: row[5],
            USEquity: row[6],
            EAFEEquity: row[7],
            GlobalEquity: row[8],
            EmergingMarketEquity: row[9],
            USREITs: row[10],
            GlobalREITs: row[11],
            USCoreFixedIncome: row[12],
            GlobalCoreFixedIncome: row[13],
            BankLoans: row[14],
            USHighYieldFixedIncome: row[15],
            GlobalHighYieldFixedIncome: row[16],
            EmergingMarketsDebt: row[17],
            Treasuries: row[18],
            MunicipalFixedIncome: row[19],
            AbsoluteReturn: row[20],
            EquityHedgeFunds: row[21],
            CreditHedgeFunds: row[22],
            MacroHedgeFunds: row[23],
            PrivateEquity: row[24],
            VentureCapital: row[25],
            PrivateDebt: row[26],
            PrivateRealEstate: row[27],
            Commodities: row[28],
            RealAssets: row[29],
            CashEquivalents: row[30],

          };
          accountsData.push(account);

        });
        await excelImportService.getAccountExcelSet(accountsData);
      });

      //performance data
      await xlsxFile('./OCIO.xlsx',  { sheet: 2 }).then(async (rows) => {
        let fileColumn = rows[0];
        if(fileColumn.includes('AccountID') && fileColumn.includes('Period') && fileColumn.includes('value')){
          rows.shift();
          const  performances = [];
          rows.forEach((row) => {
            const performance = {
              AccountID: row[0],
              Period: row[1],
              value: row[2],
            };
            performances.push(performance);
          });
          await excelImportService.getPerformanceExcelSet(performances);
        }
      });
      break;
    default:
      console.log('Command not found');
      process.exit(1);
  }

  //await application.close();
  process.exit(0);
}

bootstrap();
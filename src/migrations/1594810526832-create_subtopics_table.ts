import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createSubtopicsTable1594810526832 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'subtopics',
                columns: [
                    {
                        name: 'id',
                        type: 'int',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    {
                        name: 'name',
                        type: 'varchar',
                        length: '191',
                        isNullable: true,
                    },
                    {
                        name: 'description',
                        type: 'text',
                        isNullable: true,
                    },
                    {
                        name: 'topic_id',
                        type: 'int',
                        isNullable: true,
                    },
                    {
                        name: 'level_id',
                        type: 'int',
                        isNullable: true,
                    },
                    {
                        name: 'created_at',
                        type: 'datetime',
                        default: 'CURRENT_TIMESTAMP',
                    },
                    {
                        name: 'updated_at',
                        type: 'datetime',
                        default: 'CURRENT_TIMESTAMP',
                    },
                ],
            }),
            true,
        );
        await queryRunner.createForeignKey("subtopics", new TableForeignKey({
            columnNames: ["topic_id"],
            referencedColumnNames: ["id"],
            referencedTableName: "topics",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const table = await queryRunner.getTable("subtopics");

        const subTopicsForeignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("topic_id") !== -1);
        await queryRunner.dropForeignKey("subtopics", subTopicsForeignKey);

        await queryRunner.dropTable('subtopics', true);
    }

}

import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createUserQuizTable1594987778962 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'user_quiz',
                columns: [
                    {
                        name: 'id',
                        type: 'int',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    {
                        name: 'user_id',
                        type: 'int',
                        isNullable: true,
                    },
                    {
                        name: 'quiz_id',
                        type: 'int',
                        isNullable: true,
                    },
                    {
                        name: 'answer',
                        type: 'varchar',
                        isNullable: true,
                    },
                    {
                        name: 'gain_weightage',
                        type: 'int',
                        isNullable: true,
                    },
                    {
                        name: 'created_at',
                        type: 'datetime',
                        default: 'CURRENT_TIMESTAMP',
                    },
                    {
                        name: 'updated_at',
                        type: 'datetime',
                        default: 'CURRENT_TIMESTAMP',
                    },
                ],
            }),
            true,
        );
        await queryRunner.createForeignKey("user_quiz", new TableForeignKey({
            columnNames: ["user_id"],
            referencedColumnNames: ["id"],
            referencedTableName: "users",
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey("user_quiz", new TableForeignKey({
            columnNames: ["quiz_id"],
            referencedColumnNames: ["id"],
            referencedTableName: "quiz",
            onDelete: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const table = await queryRunner.getTable("user_quiz");

        const userForeignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("user_id") !== -1);
        await queryRunner.dropForeignKey("user_quiz", userForeignKey);

        const quizForeignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("quiz_id") !== -1);
        await queryRunner.dropForeignKey("user_quiz", quizForeignKey);


        await queryRunner.dropTable('user_quiz', true);
    }

}

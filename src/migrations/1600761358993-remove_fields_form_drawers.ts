import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from "typeorm";

export class removeFieldsFormDrawers1600761358993 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {

        const table = await queryRunner.getTable("drawers");

        const levelForeignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("level_id") !== -1);
        await queryRunner.dropForeignKey("drawers", levelForeignKey);

        const subTopicForeignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("subtopic_id") !== -1);
        await queryRunner.dropForeignKey("drawers", subTopicForeignKey);

        await queryRunner.dropColumn("drawers", "level_id");
        
        await queryRunner.dropColumn("drawers", "subtopic_id");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {

        await queryRunner.addColumns('drawers', [
            new TableColumn({
              name: 'level_id',
              type: 'int',
              isNullable: true,
            }),
          ]
        );
        await queryRunner.addColumns('drawers', [
            new TableColumn({
              name: 'subtopic_id',
              type: 'int',
              isNullable: true,
            }),
          ]
        );

        await queryRunner.createForeignKey("drawers", new TableForeignKey({
            columnNames: ["level_id"],
            referencedColumnNames: ["id"],
            referencedTableName: "level",
            onDelete: "CASCADE"
        }));

        await queryRunner.createForeignKey("drawers", new TableForeignKey({
            columnNames: ["subtopic_id"],
            referencedColumnNames: ["id"],
            referencedTableName: "subtopics",
            onDelete: "CASCADE"
        }));
    }

}

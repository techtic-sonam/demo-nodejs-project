import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm";

export class drawerSimilarStatment1595592302433 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'drawers_similar_statement',
                columns: [
                    {
                        name: 'id',
                        type: 'int',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    {
                        name: 'drawers_id',
                        type: 'int',
                        isNullable: false,
                    },
                    {
                        name: 'statement_id',
                        type: 'int',
                        isNullable: false,
                    },
                    {
                        name: 'created_at',
                        type: 'datetime',
                        default: 'CURRENT_TIMESTAMP',
                    },
                    {
                        name: 'updated_at',
                        type: 'datetime',
                        default: 'CURRENT_TIMESTAMP',
                    },
                ],
            }),
            true,
        );

       /*  await queryRunner.createForeignKey("drawers", new TableForeignKey({
            columnNames: ["statement_id"],
            referencedColumnNames: ["id"],
            referencedTableName: "similar_statements",
            onDelete: "CASCADE"
        })); */

       /*  await queryRunner.createForeignKey("drawers", new TableForeignKey({
            columnNames: ["drawers_id"],
            referencedColumnNames: ["id"],
            referencedTableName: "drawers",
            onDelete: "CASCADE"
        })); */
    }



    public async down(queryRunner: QueryRunner): Promise<void> {
        const table = await queryRunner.getTable("drawers_similar_statement");

        const levelForeignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("statement_id") !== -1);
        await queryRunner.dropForeignKey("drawers_similar_statement", levelForeignKey);

        const subTopicForeignKey = table.foreignKeys.find(fk => fk.columnNames.indexOf("drawers_id") !== -1);
        await queryRunner.dropForeignKey("drawers_similar_statement", subTopicForeignKey);

        await queryRunner.dropTable('drawers_similar_statement', true);
    }

}

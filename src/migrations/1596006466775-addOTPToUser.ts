import {MigrationInterface, QueryRunner, TableColumn} from "typeorm";

export class addOTPToUser1596006466775 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn('users', new TableColumn({
            name: 'otp',
            type: 'varchar',
            isNullable: true
        }));

        await queryRunner.addColumn('users', new TableColumn({
            name: 'isVerified',
            type: 'boolean',
            isNullable: true
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('users', true);
    }

}

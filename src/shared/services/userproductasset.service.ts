import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserProductAsset } from "../../modules/userproductasset/userproductasset.entity";
import { ConfigService } from '@nestjs/config';

@Injectable()
export class UserProductAssetService {
  private configService: any;

  constructor(
    @InjectRepository(UserProductAsset) private readonly userProductAssetRepository: Repository<UserProductAsset>,
  ) {
    this.configService = new ConfigService();
  }

  async addUpdateAsset(payload,user_id): Promise<any> {
    try {
      let userProductAsset = new UserProductAsset();
      if(payload.name){
        userProductAsset.name = payload.name;
      }
      if(payload.period){
        userProductAsset.period = payload.period;
      }
      if(payload.usfixedincome){
        userProductAsset.USCoreFixedIncome = payload.usfixedincome;
      }
      if(payload.usequity){
        userProductAsset.USEquity = payload.usequity;
      }
      if(payload.eafeequity){
        userProductAsset.EAFEEquity = payload.eafeequity;
      }
      if(payload.realassets){
        userProductAsset.RealAssets = payload.realassets;
      }
      if(payload.cashequivalents){
        userProductAsset.CashEquivalents = payload.cashequivalents;
      }
      if(payload.equityhedgefunds){
        userProductAsset.EquityHedgeFunds = payload.equityhedgefunds;
      }
      if(payload.globalreits){
        userProductAsset.GlobalREITs = payload.globalreits;
      }
      if(payload.globalfixedincome){
        userProductAsset.GlobalCoreFixedIncome = payload.globalfixedincome;
      }
      if(payload.bankloans){
        userProductAsset.BankLoans = payload.bankloans;
      }
      if(payload.usyieldincome){
        userProductAsset.USHighYieldFixedIncome = payload.usyieldincome;
      }
      if(payload.globalyieldincome){
        userProductAsset.GlobalHighYieldFixedIncome = payload.globalyieldincome;
      }
      if(payload.emrngmarketsdebt){
        userProductAsset.EmergingMarketsDebt = payload.emrngmarketsdebt;
      }
      if(payload.emrngmarketequity){
        userProductAsset.EmergingMarketEquity = payload.emrngmarketequity;
      }
      if(payload.munciplfixedincome){
        userProductAsset.MunicipalFixedIncome = payload.munciplfixedincome;
      }
      if(payload.absolutereturn){
        userProductAsset.AbsoluteReturn = payload.absolutereturn;
      }
      if(payload.usreits){
        userProductAsset.USREITs = payload.usreits;
      }
      if(payload.credithedgefunds){
        userProductAsset.CreditHedgeFunds = payload.credithedgefunds;
      }
      if(payload.macrohedgefunds){
        userProductAsset.MacroHedgeFunds = payload.macrohedgefunds;
      }
      if(payload.privateequity){
        userProductAsset.PrivateEquity = payload.privateequity;
      }
      if(payload.venturecapital){
        userProductAsset.VentureCapital = payload.venturecapital;
      }
      if(payload.privatedebt){
        userProductAsset.PrivateDebt = payload.privatedebt;
      }
      if(payload.privaterealestate){
        userProductAsset.PrivateRealEstate = payload.privaterealestate;
      }
      if(payload.commodities){
        userProductAsset.Commodities = payload.commodities;
      }userProductAsset
      if(payload.globalequity){
        userProductAsset.GlobalEquity = payload.globalequity;
      }
      if(payload.treasuries){
        userProductAsset.Treasuries = payload.treasuries;
      }
      userProductAsset.user_id = user_id;

      if (payload.id) {
        await this.userProductAssetRepository.update(payload.id, userProductAsset);
        let find = await this.userProductAssetRepository.findOne({ id: payload.id });
        return find;
      }else{
        let data = await this.userProductAssetRepository.save(userProductAsset);
        return data;
      }

    }catch (error) {
      throw error;
    }
  }

  async getAll() {
    try {
      const response = await this.userProductAssetRepository.createQueryBuilder('user_products_assets')
        .getMany();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async findOne(where: Object): Promise<UserProductAsset> {
    return this.userProductAssetRepository.findOne({ where: where});
  }

  async delete(id) {
    await this.userProductAssetRepository.delete({ id: id })
  }

  async deleteProductByUserId(userId) {
    await this.userProductAssetRepository.delete({ user_id: userId })
  }

}
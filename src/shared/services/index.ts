import { UsersService } from "./users.service";
import { UserAccountService } from "./useraccount.service";
import { DashboardService } from "./dashboard.service";
import { ExcelImportService } from "./excelimport.service";
import { PerformanceService } from "./performance.service";
import { EmailService } from "./email.service";
import { UserProductService } from "./userproduct.service";
import { UserProductAssetService } from "./userproductasset.service";


export { UsersService } from "./users.service";
export { UserAccountService } from "./useraccount.service";
export { DashboardService } from "./dashboard.service";
export { ExcelImportService } from "./excelimport.service";
export { PerformanceService } from "./performance.service";
export { UserProductService } from "./userproduct.service";
export { UserProductAssetService } from "./userproductasset.service";

const Services: any = [
  UsersService,
  UserAccountService,
  DashboardService,
  ExcelImportService,
  PerformanceService,
  EmailService,
  UserProductService,
  UserProductAssetService
];

export { Services };

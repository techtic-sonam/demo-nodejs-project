import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { UserAccount } from "../../modules/useraccount/useraccount.entity";
import { UserAccountPerformance } from '../../modules/useraccount/useraccountsperformance.entity';


@Injectable()
export class UserAccountService {
  private configService: any;

  constructor(
    @InjectRepository(UserAccount) private readonly useraccountRepository: Repository<UserAccount>,
    @InjectRepository(UserAccountPerformance) private readonly accountperformance: Repository<UserAccountPerformance>
  ) {
    this.configService = new ConfigService();
  }

  async accountAddUpdate(payload,user_id): Promise<any> {
    try {
      let account = new UserAccount();
      account.user_id = user_id;
      let account_id;
      if(payload.id){
        account_id = payload.id;
        account.account_name = payload.name;
        await this.useraccountRepository.update(account_id, account);
        //delete old records
        await this.accountperformance.createQueryBuilder()
                                      .delete()
                                      .from(UserAccountPerformance)
                                      .where("account_id = :actid", { actid: account_id })
                                      .execute();

      }else{
        account.account_name = payload.name;
        let accountData = await this.useraccountRepository.save(account);
        account_id = accountData['id'];
      }

      for(var i=0; i< payload.performance.length; i++ ){
        let element = payload.performance[i];
        if(element['q1']){
          let performance = new UserAccountPerformance();
          performance.account_id = account_id;
          performance.period = element['year']+'03';
          performance.value = element['q1'];
          await this.accountperformance.save(performance);
        }
        if(element['q2']){
          let performance = new UserAccountPerformance();
          performance.account_id = account_id;
          performance.period = element['year']+'06';
          performance.value = element['q2'];
          await this.accountperformance.save(performance);
        }
        if(element['q3']){
          let performance = new UserAccountPerformance();
          performance.account_id = account_id;
          performance.period = element['year']+'09';
          performance.value = element['q3'];
          await this.accountperformance.save(performance);
        }
        if(element['q4']){
          let performance = new UserAccountPerformance();
          performance.account_id = account_id;
          performance.period = element['year']+'12';
          performance.value = element['q4'];
          await this.accountperformance.save(performance);
        }
      }
       return payload.performance;
    }catch (error) {
      throw error;
    }
  }

  async getAll() {
    try {
      const response = await this.useraccountRepository.createQueryBuilder('user_products')
        .getMany();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async findOne(where: Object): Promise<UserAccount> {
     return this.useraccountRepository.findOne({where:where});
  }

  async getByAccountId(accountId) {
    try {

      let performances = await this.accountperformance.createQueryBuilder('user_accounts_performance')
         .where("account_id = :id", { id: accountId })
        .getMany();

      let account = await this.useraccountRepository.createQueryBuilder('user_accounts')
                      .where("id = :id", { id: accountId })
                    .getOne();

      return {"account":account,"performances":performances}

    } catch (error) {
      throw error;
    }
  }


}

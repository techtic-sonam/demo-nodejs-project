import { Injectable,BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import * as crypto from 'crypto';
import { User } from "../../modules/user/user.entity";
import * as _ from 'underscore';
import { ConfigService } from '@nestjs/config';
import { Dropdown } from '../../modules/user/dropdown.entity';
import { Performance } from '../../modules/performance/performance.entity';

@Injectable()
export class PerformanceService {
  private configService: any;

  constructor(
    @InjectRepository(Performance) private readonly performanceRepository: Repository<Performance>

  ) {
    this.configService = new ConfigService();
  }


  async getAll() {
    try {

      let response = await getConnection().query(`select distinct period from performance where period >= 201003 order by period desc;`);
      
      let arrayPush = [];

      response.forEach(element => {
        let currentYear = Math.trunc(element.period/100);
        let currentMonth = element.period % 100;
        let finalMonth = '';

        if(currentMonth == 3) {
          finalMonth = "Q1";
        } else if(currentMonth == 6) {
          finalMonth = "Q2";
        } else if(currentMonth == 9) {
          finalMonth = "Q3";
         } else if(currentMonth == 12) {
          finalMonth = "Q4";
        }else{
          return;
        }
        let array = {"year":finalMonth+'/'+currentYear, "period" : element.period };
        arrayPush.push(array);
      });
      return arrayPush;
    } catch (error) {
      throw error;
    }
  }


}

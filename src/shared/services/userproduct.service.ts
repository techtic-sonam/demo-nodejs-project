import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserProduct } from "../../modules/userproduct/userproduct.entity";
import { ConfigService } from '@nestjs/config';

@Injectable()
export class UserProductService {
  private configService: any;

  constructor(
    @InjectRepository(UserProduct) private readonly userProductRepository: Repository<UserProduct>,

  ) {
    this.configService = new ConfigService();
  }

  async create(payload,user_id): Promise<any> {
    try {
      let userProduct = new UserProduct();
      if(payload.name){
        userProduct.name = payload.name;
      }
      if(payload.period){
        userProduct.period = payload.period;
      }

      if(payload.qtr){
        userProduct.three_month = payload.qtr;
      }
      if(payload.ytd){
        userProduct.ytd = payload.ytd;
      }
      if(payload.one_year){
        userProduct.one_year = payload.one_year;
      }
      if(payload.three_year){
        userProduct.three_year = payload.three_year;
      }
      if(payload.five_year){
        userProduct.five_year = payload.five_year;
      }
      if(payload.seven_year){
        userProduct.seven_year = payload.seven_year;
      }
      if(payload.ten_year){
        userProduct.ten_year = payload.ten_year;
      }

      userProduct.user_id = user_id;

      if (payload.id) {
        await this.userProductRepository.update(payload.id, userProduct);
        let find = await this.userProductRepository.findOne({ id: payload.id });
        return find;
      }else{
        let data = await this.userProductRepository.save(userProduct);
        return data;
      }

    }catch (error) {
      throw error;
    }
  }

  async getAll() {
    try {
      const response = await this.userProductRepository.createQueryBuilder('user_products')
        .getMany();
      return response;
    } catch (error) {
      throw error;
    }
  }

  async findOne(where: Object): Promise<UserProduct> {
    return this.userProductRepository.findOne({ where: where});
  }

  async delete(id) {
    await this.userProductRepository.delete({ id: id })
  }

  async deleteProductByUserId(userId) {
    await this.userProductRepository.delete({ user_id: userId })
  }

  async addAsset(payload,user_id): Promise<any> {
    try {
      let userProduct = new UserProduct();
      if(payload.name){
        userProduct.name = payload.name;
      }
      return '';

    }catch (error) {
      throw error;
    }
  }
}
import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer'
import { ConfigService } from '@nestjs/config';
import { baseUrl } from '../../shared/helpers/utils';


@Injectable()
export class EmailService {
    private configService: any;

    constructor(
        private readonly mailerService: MailerService,
    ) {
        this.configService = new ConfigService();
    }
    public async userForgotPassword(data: any = {}): Promise<any> {
        let logo = baseUrl('assets/images/logo.jpeg');
        data = Object.assign({}, data, this.configService);
        let url = 'reset-password/' + data.context.uuid;
        data.context.reset_password_url = baseUrl(url);
        console.log(data,"datadata");
        await this.mailerService.sendMail({
          to: data.to, //List of receivers
          subject: 'Forgot Password - ' + this.configService.get('APP_NAME'), //Subject line
          html: '<b>Hello '+data.context.first_name+" "+data.context.last_name+'</b>,<br/><br/>'+
                'You are receiving this email because we received a password reset request for your account <br/> Click the button below to reset your password'+
                '<br/><p><a href="'+data.context.reset_password_url+'">Reset Password Link<a></p>'
        });
    }


}

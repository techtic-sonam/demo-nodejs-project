import { Injectable, BadRequestException, Inject, forwardRef, NotAcceptableException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection,Repository } from 'typeorm';
import * as crypto from 'crypto';
import { User } from "../../modules/user/user.entity";
import * as _ from 'underscore';
import { ConfigService } from '@nestjs/config';
import { Dropdown } from '../../modules/user/dropdown.entity';
import { becrypt } from '../../common/utils';
import { UserActivity } from '../../modules/user/useractivity.entity';
import { EmailService } from './email.service';
import { AnyARecord } from 'node:dns';



var randomize = require('randomatic');

@Injectable()
export class UsersService {
  private configService: any;

  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Dropdown) private readonly licenceRepository: Repository<Dropdown>,
    @InjectRepository(UserActivity) private readonly userActivityRepository: Repository<UserActivity>,
    @Inject(forwardRef(() => EmailService)) private readonly emailService: EmailService,

  ) {
    this.configService = new ConfigService();
  }

  async withRelations(
    conditions: Object,
    relations: Array<any> = [],
  ): Promise<User> {
    return await this.userRepository.findOne({
      where: conditions,
      relations: relations,
    });
  }

  async findOne(where: Object, relations: Array<any> = []): Promise<User> {
    return this.userRepository.findOne({ where: where, relations: relations });
  }

  async loginUser(where: Object){
    let user = await this.findOne(where, []);
    return user;
  }

  async getByEmailAndPass(email: string, password: string) {

    const passHash = crypto.createHmac('sha256', password).digest('hex');
    return await this.userRepository
      .createQueryBuilder('user')
      .where(
        '(user.email = :email or user.phone_no = :email) and user.password = :password',
      )
      .setParameter('email', email)
      .setParameter('password', passHash)
      .getOne();
  }


  async create(payload): Promise<any> {
    try {
      let user = new User();

      if (payload.first_name) {
          user.first_name = payload.first_name;
      }

      if (payload.last_name) {
          user.last_name = payload.last_name;
      }

      if (payload.email) {
          user.email = payload.email;
          let find = await this.userRepository.findOne({ email: user.email });
          if (find && find.id != payload.id) {
            throw new BadRequestException('This email is already used.')
          }
      }


      if (payload.company) {
          user.company = payload.company;
      }

     if (payload.license) {
          user.license = payload.license;
      }

      if (payload.phone_no) {
          user.phone_no = payload.phone_no;
      }

      var current_date = (new Date()).valueOf().toString();
      var random = Math.random().toString();

      if (payload.id) {
        if (payload.Password) {
          user.password = crypto.createHmac('sha256', payload.password).digest('hex');
        }
        await this.userRepository.update(payload.id, user);
      } else {
          if (payload.Password) {
            user.password = crypto.createHmac('sha256', payload.password).digest('hex');
          }else{
            user.password = "";
          }
          user.role_id = 2;
          user.uuid = crypto.createHash('sha1').update(current_date + random).digest('hex');
          let data = await this.userRepository.save(user);
          payload.id = data.id;
      }

      return await this.findOne({ uuid: payload.uuid });
    } catch (error) {
      throw error;
    }
  }


  async getAll() {
    try {
      let user_role_id = 2;
      let sql = "SELECT u.*, count(ua.activity) as login_count ,dr.display_text AS license_text FROM users as u "
                +"LEFT JOIN (SELECT * FROM user_activities  as ua WHERE ua.activity = 'login') ua ON (ua.user_id = u.id) "
                +"LEFT JOIN dropdowns as dr on dr.id = u.license "
                +"where u.role_id = "+user_role_id+" "
                +"GROUP BY u.id  ORDER BY u.id ASC";
     return await getConnection().query(sql);

    } catch (error) {
      throw error;
    }
  }

  async getDropdownByParam(fieldType) {
    try {
        if (fieldType) {
          return await this.licenceRepository.createQueryBuilder('dropdowns')
            .where("field = :fieldType", { fieldType: fieldType })
            .getMany();

        } else {
            return await this.licenceRepository.createQueryBuilder('dropdowns')
            .getMany();
        }
    } catch (error) {
        throw error;
    }
}

  async delete(id) {
    await this.userRepository.delete({ id: id })
  }

  async getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  async changePassword(loginUser,payload): Promise<any> {
    let encrptOldPwd = crypto.createHmac('sha256', payload.old_password).digest('hex');
    let matchPassword = await this.userRepository.count({ id: loginUser,password: encrptOldPwd });
    if (matchPassword > 0) {
      await this.userRepository.update(loginUser, { password: becrypt(payload.password) })
        .then(res => {
          return true;
        })
        .catch(error => {
          throw new BadRequestException(error);
        });
    } else {
      throw new NotAcceptableException(
        'old password is not match.',
      );
    }
  }


  async resetPassword(payload): Promise<any> {
    console.log(payload,"payloadpayloadpayload");
    let matchPassword = await this.userRepository.findOne({ uuid: payload.uuid });
    if (matchPassword) {
      await this.userRepository.update(matchPassword.id, { password: becrypt(payload.password) })
        .then(res => {
          return true;
        })
        .catch(error => {
          throw new BadRequestException(error);
        });
    } else {
      throw new NotAcceptableException(
        'user not found.',
      );
    }
  }


  async setUserPassword(payload){
    try {
      if (payload) {
        let userByuuid = await this.userRepository.createQueryBuilder('users')
        .addSelect('users.password')
        .where("uuid = :uuid", { uuid: payload.uuid })
        .getOne();
        if(userByuuid ){
          if(userByuuid['password'] == ""){
            payload.id = (await userByuuid).id;
            let user = new User();
            user.password = crypto.createHmac('sha256', payload.password).digest('hex');
            await this.userRepository.update(payload.id, user);
            return 1;
          }else{
            return 2;//"User has already set password";
          }
        }else{
          return 3; //"Error! User does not exists";
        }
      }
    } catch (error) {
        throw error;
    }
  }

  async userLoginActivity(userId){
    let userActivity = new UserActivity();
    userActivity.user_id = userId;
    userActivity.activity = 'login';
    await this.userActivityRepository.save(userActivity);
    let loginCount =  await this.userActivityRepository.count({ user_id: userId, activity: 'login' });
    return loginCount;
  }

  async sendForgotPasswordEmail(payload): Promise<any> {
    let email = payload.email;
    let context: any = await this.findOne({ email: email });
    
    
    context.request_type = (payload.type) ? payload.type : 'app';
    context.name = context.first_name;
    let response_message = '';
    await this.emailService
      .userForgotPassword({ to: email, context: context })
      .then(res => { })
      .catch(error => {
        throw new BadRequestException(error);
      });
    response_message = 'A new password has been sent to your email.';

    return response_message;
  }

}

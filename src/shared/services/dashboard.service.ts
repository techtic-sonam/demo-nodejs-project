import { Injectable,BadRequestException, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, Repository, UsingJoinTableIsNotAllowedError } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { UserProduct } from "../../modules/userproduct/userproduct.entity";
import { Dropdown } from '../../modules/user/dropdown.entity';
import { Accounts } from '../../modules/account/account.entity';
import e from 'express';
import { ConfigurationServicePlaceholders } from 'aws-sdk/lib/config_service_placeholders';
import { resourceLimits } from 'worker_threads';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
import { ConsoleWriter } from 'istanbul-lib-report';
import * as moment from 'moment';
import { UserProductAsset } from '../../modules/userproductasset/userproductasset.entity';


@Injectable()
export class DashboardService {
  private configService: any;

  constructor(
    @InjectRepository(UserProduct) private readonly userProductRepository: Repository<UserProduct>,
    @InjectRepository(Dropdown) private readonly dropdownRepository: Repository<Dropdown>,
    @InjectRepository(Accounts) private readonly accountsRepository: Repository<Accounts>,
    @InjectRepository(UserProductAsset) private readonly userProductAssetRepository: Repository<UserProductAsset>,
  ) {
    this.configService = new ConfigService();
  }

  async search(payload,loginUser): Promise<any> {

    let selectedDate = payload.date;
    let size = payload.size;
    let type = payload.type;
    let risk = payload.risk;
    let lowerSizeLimit;
    let upperSizeLimit;
    var chartData = [];
    let lastVal;
    let firstVal;

    if(size.length > 0){
      let sizeall = [];

      size.map(function(el) {
        let split = el.split('-');
        firstVal = split[0];
        lastVal = split[1];
        return sizeall.push(lastVal, firstVal);
      });

      lowerSizeLimit = Math.min.apply(Math, sizeall);
      upperSizeLimit = Math.max.apply(Math, sizeall);


    } else {
      lowerSizeLimit = 0;
      upperSizeLimit = 50;
    }


    let dates = ['QTR', 'YTD', '1 Year', '3 Years', '5 Years', '7 Years', '10 Years'];

    //TO-DO: these need to query the products that belong to the logged in user
    let perfPeriod = 202103;

    let queryDate = moment(selectedDate, 'YYYYMM');
    let minDate = queryDate.subtract(39, 'Q').format('YYYYMM');

    let perfQuery = `SELECT a.AccountId, performance.Period 'period',performance.value FROM accounts a
                    JOIN performance ON a.AccountID = performance.AccountID  
                        where performance.Period >=` + minDate + ` and a.period = ` + perfPeriod;


    let assetsQuery = `
    select
    sum(USEquity) 'USEquity',
    sum(EAFEEquity) 'EAFEEquity',
    sum(GlobalEquity) 'GlobalEquity',
    sum(EmergingMarketEquity) 'EmergingMarketEquity',
    sum(USREITs) 'USREITs',
    sum(GlobalREITs) 'GlobalREITs',
    sum(USCoreFixedIncome) 'USCoreFixedIncome',
    sum(BankLoans) 'BankLoans',
    sum(USHighYieldFixedIncome) 'USHighYieldFixedIncome',
    sum(GlobalHighYieldFixedIncome) 'GlobalHighYieldFixedIncome',
    sum(EmergingMarketsDebt) 'EmergingMarketsDebt',
    sum(Treasuries) 'Treasuries',
    sum(MunicipalFixedIncome) 'MunicipalFixedIncome',
    sum(AbsoluteReturn) 'AbsoluteReturn',
    sum(CreditHedgeFunds) 'CreditHedgeFunds',
    sum(MacroHedgeFunds) 'MacroHedgeFunds',
    sum(PrivateEquity) 'PrivateEquity',
    sum(VentureCapital) 'VentureCapital',
    sum(PrivateDebt) 'PrivateDebt',
    sum(PrivateRealEstate) 'PrivateRealEstate',
    sum(Commodities) 'Commodities',
    sum(RealAssets) 'RealAssets',
    sum(CashEquivalents) 'CashEquivalents',
    sum(EquityHedgeFunds) 'EquityHedgeFunds',
    sum(GlobalCoreFixedIncome) 'GlobalCoreFixedIncome',
    max(USEquity) 'maxUSEquity',
    max(EAFEEquity) 'maxEAFEEquity',
    max(GlobalEquity) 'maxGlobalEquity',
    max(EmergingMarketEquity) 'maxEmergingMarketEquity',
    max(USREITs) 'maxUSREITs',
    max(GlobalREITs) 'maxGlobalREITs',
    max(USCoreFixedIncome) 'maxUSCoreFixedIncome',
    max(BankLoans) 'maxBankLoans',
    max(USHighYieldFixedIncome) 'maxUSHighYieldFixedIncome',
    max(GlobalHighYieldFixedIncome) 'maxGlobalHighYieldFixedIncome',
    max(EmergingMarketsDebt) 'maxEmergingMarketsDebt',
    max(Treasuries) 'maxTreasuries',
    max(MunicipalFixedIncome) 'maxMunicipalFixedIncome',
    max(AbsoluteReturn) 'maxAbsoluteReturn',
    max(CreditHedgeFunds) 'maxCreditHedgeFunds',
    max(MacroHedgeFunds) 'maxMacroHedgeFunds',
    max(PrivateEquity) 'maxPrivateEquity',
    max(VentureCapital) 'maxVentureCapital',
    max(PrivateDebt) 'maxPrivateDebt',
    max(PrivateRealEstate) 'maxPrivateRealEstate',
    max(Commodities) 'maxCommodities',
    max(RealAssets) 'maxRealAssets',
    max(CashEquivalents) 'maxCashEquivalents',
    max(EquityHedgeFunds) 'maxEquityHedgeFunds',
    max(GlobalCoreFixedIncome) 'maxGlobalCoreFixedIncome',
    min(USEquity) 'minUSEquity',
    min(EAFEEquity) 'minEAFEEquity',
    min(GlobalEquity) 'minGlobalEquity',
    min(EmergingMarketEquity) 'minEmergingMarketEquity',
    min(USREITs) 'minUSREITs',
    min(GlobalREITs) 'minGlobalREITs',
    min(USCoreFixedIncome) 'minUSCoreFixedIncome',
    min(BankLoans) 'minBankLoans',
    min(USHighYieldFixedIncome) 'minUSHighYieldFixedIncome',
    min(GlobalHighYieldFixedIncome) 'minGlobalHighYieldFixedIncome',
    min(EmergingMarketsDebt) 'minEmergingMarketsDebt',
    min(Treasuries) 'minTreasuries',
    min(MunicipalFixedIncome) 'minMunicipalFixedIncome',
    min(AbsoluteReturn) 'minAbsoluteReturn',
    min(CreditHedgeFunds) 'minCreditHedgeFunds',
    min(MacroHedgeFunds) 'minMacroHedgeFunds',
    min(PrivateEquity) 'minPrivateEquity',
    min(VentureCapital) 'minVentureCapital',
    min(PrivateDebt) 'minPrivateDebt',
    min(PrivateRealEstate) 'minPrivateRealEstate',
    min(Commodities) 'minCommodities',
    min(RealAssets) 'minRealAssets',
    min(CashEquivalents) 'minCashEquivalents',
    min(EquityHedgeFunds) 'minEquityHedgeFunds',
    min(GlobalCoreFixedIncome) 'minGlobalCoreFixedIncome',
    count(*) 'observations'
    from accounts a
    where a.Period = ` + selectedDate;

    //CHANGE TO OR LOGIC
    //Can only choose 1 size
    if (size.length > 0){
      perfQuery  = perfQuery  + ` and a.AccountSize >= ` + lowerSizeLimit + ` and a.AccountSize < ` +upperSizeLimit
      assetsQuery  = assetsQuery  + ` and a.AccountSize >= ` + lowerSizeLimit + ` and a.AccountSize < ` +upperSizeLimit
    }

    //can choose multiple types
    if (type.length > 0){

     type =  type.map(function(el) {
        return '\'' + el + '\'';
      })

      perfQuery  = perfQuery +  ` and a.AccountId in (	select AccountId from account_universe_mapping where universe in (` + type.join() + `) and period = ` +  perfPeriod + `)`;
      assetsQuery  = assetsQuery +  ` and a.AccountId in (	select AccountId from account_universe_mapping where universe in (` + type.join() + `) and period = ` +  selectedDate + `)`;
    }

    if (risk.length > 0){
      risk =  risk.map(function(el) {
        return '\'' + el + '\'';
      })

      perfQuery  = perfQuery +  ` and a.AccountId in (	select AccountId from account_universe_mapping where universe in (` + risk.join() + `) and period = ` +  perfPeriod + `)`;
      assetsQuery  = assetsQuery +  ` and a.AccountId in (	select AccountId from account_universe_mapping where universe in (` + risk.join() + `) and period = ` +  selectedDate + `)`;
    }

    perfQuery = perfQuery + ` ORDER by performance.Period`;


    let perfData = await getConnection().query(perfQuery);
    let assetsData = await getConnection().query(assetsQuery);

    let assetObservation = assetsData[0]['observations'];

    const filterData = (arr, start, end) => {
      return arr.filter(row => row.period >= start && row.period <= end);
    }

    const observations = (arr, accountId) => {
      return arr.filter(row => row.AccountId === accountId);
    }

    let currentYear = Math.trunc(selectedDate/100);
    let currentMonth = selectedDate % 100;

    var requiredObservations = 0;
    var pow = 1;

    var percentiles = [];
    var raw_performance = [];

    dates.forEach(element => {
      var filteredData = [];

      let date = moment(selectedDate, 'YYYYMM');
      let calculatedDate = selectedDate;

      switch(element){
        case 'QTR':
            requiredObservations = 1;
            calculatedDate = selectedDate;
        break;
        case 'YTD':

            let start = parseInt(currentYear + '03');

            switch(currentMonth){
              case 3:
                  requiredObservations = 1;
                  break;
              case 6:
                  requiredObservations = 2;
                  break;
              case 9:
                  requiredObservations = 3;
              break;
              default:
                  requiredObservations = 4;
              break;
            }
            calculatedDate = start;

        break;
        case '1 Year':
            requiredObservations = 4;
            calculatedDate = date.subtract(3, 'Q').format('YYYYMM');

        break;
        case '3 Years':
            pow = 3;
            requiredObservations = 12;
            calculatedDate = date.subtract(11, 'Q').format('YYYYMM');

        break;
        case '5 Years':
            pow = 5;
            requiredObservations = 20;
            calculatedDate = date.subtract(19, 'Q').format('YYYYMM');

        break;
        case '7 Years':
            pow = 7;
            requiredObservations = 28;
            calculatedDate = date.subtract(27, 'Q').format('YYYYMM');

        break;
        case '10 Years':
            pow = 10;
            requiredObservations = 40;
            calculatedDate = date.subtract(39, 'Q').format('YYYYMM');

        break;
      }

      filteredData = filterData(perfData, calculatedDate, selectedDate);

      let accounts = filteredData.map(function(data){return data.AccountId;});
      let accountObservations = 0;

      accounts = [...new Set(accounts)];


      let performance = [];

      accounts.forEach(accountId => {
        let accountData = observations(filteredData, accountId);

        if (accountData.length === requiredObservations){
          accountObservations += 1;
          var annualReturn = 1;
          accountData.forEach(row => {
            annualReturn *= (1 + row.value/100);
          })


         performance.push(annualReturn);
        };

      });

      //reverse the percentiles here, 5th is really 95th

      let result = {'label': element,
          'percentile_5':  Math.round(((Math.pow(this.percentile(performance, 95), (1/pow)) - 1) * 100) * 100)/100,
          'percentile_25':  Math.round(((Math.pow(this.percentile(performance, 75), (1/pow)) - 1) * 100) * 100)/100,
          'percentile_50':  Math.round(((Math.pow(this.percentile(performance, 50), (1/pow)) - 1) * 100) * 100)/100,
          'percentile_75':  Math.round(((Math.pow(this.percentile(performance, 25), (1/pow)) - 1) * 100) * 100)/100,
          'percentile_95':  Math.round(((Math.pow(this.percentile(performance, 5), (1/pow)) - 1) * 100) * 100)/100,
          'observations':   accountObservations,
          'startDate': calculatedDate
        };


      percentiles[element] = [];

      if (accountObservations < 15){
        result = {'label': element,
          'percentile_5':  null,
          'percentile_25': null,
          'percentile_50':  null,
          'percentile_75': null,
          'percentile_95': null,
          'observations':  null,
          'startDate': calculatedDate
        };
      }else{
        //calculate percentiles 1 to 100
        
        for (var i=0; i < 100; i += 1){
          percentiles[element][i] = Math.round(((Math.pow(this.percentile(performance, 100-i), (1/pow)) - 1) * 100) * 100)/100;
        }
        
        raw_performance[element] = [];
        
      }

      chartData.push(result);

    });

    //run assets logic
    var total = 0;
    var buckets = [
      {name: 'US Equity', key: 'USEquity',y: 0, max: assetsData[0]['maxUSEquity'], min: assetsData[0]['minUSEquity'] },
      {name: 'International (EAFE) Equity', key: 'EAFEEquity',y: 0, max: assetsData[0]['maxEAFEEquity'], min: assetsData[0]['minEAFEEquity']},
      {name: 'Global Equity', key: 'GlobalEquity',y: 0, max: assetsData[0]['maxGlobalEquity'], min: assetsData[0]['minGlobalEquity']},
      {name: 'Emerging Market Equity', key: 'EmergingMarketEquity',y: 0, max: assetsData[0]['maxEmergingMarketEquity'], min: assetsData[0]['minEmergingMarketEquity']},
      {name: 'US REITs', key: 'USREITs',y: 0, max: assetsData[0]['maxUSREITs'], min: assetsData[0]['minUSREITs']},
      {name: 'Global REITs', key: 'GlobalREITs',y: 0, max: assetsData[0]['maxGlobalREITs'], min: assetsData[0]['minGlobalREITs']},
      {name: 'US Core Fixed Income', key: 'USCoreFixedIncome',y: 0, max: assetsData[0]['maxUSCoreFixedIncome'], min: assetsData[0]['minUSCoreFixedIncome']},
      {name: 'Global Core Fixed Income', key: 'GlobalCoreFixedIncome',y: 0, max: assetsData[0]['maxGlobalCoreFixedIncome'], min: assetsData[0]['minGlobalCoreFixedIncome']},
      {name: 'Bank Loans', key: 'BankLoans',y: 0, max: assetsData[0]['maxBankLoans'], min: assetsData[0]['minBankLoans']},
      {name: 'US High Yield Fixed Income', key: 'USHighYieldFixedIncome',y: 0, max: assetsData[0]['maxUSHighYieldFixedIncome'], min: assetsData[0]['minUSHighYieldFixedIncome']},
      {name: 'Global High Yield Fixed Income', key: 'GlobalHighYieldFixedIncome',y: 0, max: assetsData[0]['maxGlobalHighYieldFixedIncome'], min: assetsData[0]['minGlobalHighYieldFixedIncome']},
      {name: 'Emerging Markets Debt', key: 'EmergingMarketsDebt',y: 0, max: assetsData[0]['maxEmergingMarketsDebt'], min: assetsData[0]['minEmergingMarketsDebt']},
      {name: 'Treasuries', key: 'Treasuries',y: 0, max: assetsData[0]['maxTreasuries'], min: assetsData[0]['minTreasuries']},
      {name: 'Municipal Fixed Income', key: 'MunicipalFixedIncome',y: 0, max: assetsData[0]['maxMunicipalFixedIncome'], min: assetsData[0]['minMunicipalFixedIncome']},
      {name: 'Absolute Return', key: 'AbsoluteReturn',y: 0, max: assetsData[0]['maxAbsoluteReturn'], min: assetsData[0]['minAbsoluteReturn']},
      {name: 'Equity Hedge Funds', key: 'EquityHedgeFunds',y: 0, max: assetsData[0]['maxEquityHedgeFunds'], min: assetsData[0]['minEquityHedgeFunds']},
      {name: 'Credit Hedge Funds', key: 'CreditHedgeFunds',y: 0, max: assetsData[0]['maxCreditHedgeFunds'], min: assetsData[0]['minCreditHedgeFunds']},
      {name: 'Macro Hedge Funds', key: 'MacroHedgeFunds',y: 0, max: assetsData[0]['maxMacroHedgeFunds'], min: assetsData[0]['minMacroHedgeFunds']},
      {name: 'Private Equity', key: 'PrivateEquity',y: 0, max: assetsData[0]['maxPrivateEquity'], min: assetsData[0]['minPrivateEquity']},
      {name: 'Venture Capital', key: 'VentureCapital',y: 0, max: assetsData[0]['maxVentureCapital'], min: assetsData[0]['minVentureCapital']},
      {name: 'Private Debt', key: 'PrivateDebt',y: 0, max: assetsData[0]['maxPrivateDebt'], min: assetsData[0]['minPrivateDebt']},
      {name: 'Private Real Estate', key: 'PrivateRealEstate',y: 0, max: assetsData[0]['maxPrivateRealEstate'], min: assetsData[0]['minPrivateRealEstate']},
      {name: 'Commodities', key: 'Commodities',y: 0, max: assetsData[0]['maxCommodities'], min: assetsData[0]['minCommodities']},
      {name: 'Real Assets', key: 'RealAssets',y: 0, max: assetsData[0]['maxRealAssets'], min: assetsData[0]['minRealAssets']},
      {name: 'Cash & Equivalents', key: 'CashEquivalents',y: 0, max: assetsData[0]['maxCashEquivalents'], min: assetsData[0]['minCashEquivalents']}
  ];

    //get total first
    buckets.forEach(assetclass => {
      total = total + assetsData[0][assetclass.key];
    });
    //calculate y values
    if (total > 0){
      buckets.forEach(assetclass => {
        assetclass.y = assetsData[0][assetclass.key]/total;
      });
    }
    //sorting
    buckets.sort( function ( a, b ) { return b.y - a.y; } );

    let assetProducts =  await this.userProductAssetRepository.createQueryBuilder('user_products_assets')
                        .where('user_id = :uid',{uid :loginUser })
                        .andWhere('period = :period',{period :selectedDate })
                        .getMany();

    if(assetObservation < 15 ){
      buckets = [];
      assetProducts = [];
    }


    let products =  await this.userProductRepository.createQueryBuilder('user_products')
    .where('user_id = :uid',{uid :loginUser })
    .andWhere('period = :period',{period :selectedDate })
    .getMany();


      let equity: any[] = [];
      let colorCode = ['#21362f','#800080', '#8B0000', '#FFFF00', '#ADD8E6', '#FFC0CB', '#00008B'];
      let i = 0;
      products.forEach(prd => {
      let prdObj = {
            "id"  :prd.id,
            "name":prd.name,
            "type":"scatter",
            color: colorCode[i],
            rank: [{'value': parseFloat(prd.three_month), 'rank': this.percentRank(percentiles['QTR'],parseFloat(prd.three_month) )},
                  {'value': parseFloat(prd.ytd), 'rank': this.percentRank(percentiles['YTD'],parseFloat(prd.ytd)) },
                  {'value': parseFloat(prd.one_year), 'rank': this.percentRank(percentiles['1 Year'],parseFloat(prd.one_year)) },
                  {'value': parseFloat(prd.three_year), 'rank': this.percentRank(percentiles['3 Years'],parseFloat(prd.three_year)) },
                  {'value': parseFloat(prd.five_year), 'rank': this.percentRank(percentiles['5 Years'],parseFloat(prd.five_year)) },
                  {'value': parseFloat(prd.seven_year), 'rank': this.percentRank(percentiles['7 Years'],parseFloat(prd.seven_year))},
                  {'value': parseFloat(prd.ten_year), 'rank': this.percentRank(percentiles['10 Years'],parseFloat(prd.ten_year))}],
            "data":[parseFloat(prd.three_month),
                    parseFloat(prd.ytd),
                    parseFloat(prd.one_year),
                    parseFloat(prd.three_year),
                    parseFloat(prd.five_year),
                    parseFloat(prd.seven_year),
                    parseFloat(prd.ten_year)]
            }

      equity.push(prdObj);
      i++;
      });

    let result = {"chartData":chartData,"equity":equity,"assetData":buckets,"assetProducts":assetProducts};

    return result;

  }

  sortNumber(a ,b) {
    return a - b;
  }

  percentile(array, percentile) {
    array.sort(this.sortNumber);
    let index = percentile/100. * (array.length-1);
    if (Math.floor(index) == index) {
    var result = array[index];
    } else {
    let i = Math.floor(index)
    var fraction = index - i;
    var result = array[i] + (array[i+1] - array[i]) * fraction;
    }
    return result;
  }

  percentRank(arr, v) {

    if (arr.length == 0){
      return null;
    }

    if (isNaN(v)){
      return null;
    }

    for (var i=1; i < 100; i += 2){
      if (arr[i] < v ){
        if ((i-1) == 0){
          return 1;
        }else{
          return i-1;
        }
      }
    }

    return 100;
   
}



}
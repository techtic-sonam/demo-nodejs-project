import { forwardRef, Inject, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Accounts } from '../../modules/account/account.entity';
import { Performance } from '../../modules/performance/performance.entity';

@Injectable()
export class ExcelImportService {
  constructor(
    @InjectRepository(Accounts) private readonly accountsRepository: Repository<Accounts>,
    @InjectRepository(Performance) private readonly performanceRepository: Repository<Performance>,
  ) { }

  async  getAccountExcelSet(accountData) {
    try {
      if(accountData.length > 0) {
        accountData.forEach(async (element) => {
          let findExist = await this.accountsRepository.findOne({AccountID: element.AccountID });
          if(findExist != undefined){
            await this.accountsRepository.update(findExist.id, element);
          }else{
            await this.accountsRepository.insert(element);
          }
        });

      }
      return true;
    } catch (error) {
      throw error;
    }
  }

  async  getPerformanceExcelSet(performanceData) {
    try {
      if(performanceData.length > 0) {
        await Promise.all(performanceData.map(async (element) => {
          let findExist = await this.performanceRepository.findOne({ where: { AccountID: element.AccountID , Period:element.Period} });
          if (findExist)  {
            await this.performanceRepository.createQueryBuilder('performance')
                  .update(element)
                  .where("AccountID = :actid", { actid: element.AccountID })
                  .where("Period = :prd", { prd: element.Period })
                  .execute();
          }else{
            await this.performanceRepository.insert(element);
          }
        }));
      }
      return true;
    } catch (error) {
      throw error;
    }
  }

  async chkExistData(accountData){
    try {
      let findExist = await this.accountsRepository.findOne({AccountID: accountData.AccountID });
      if(findExist != undefined){
        await this.accountsRepository.update(accountData.AccountID, accountData);
        return true;
      }
    } catch (error) {
      throw error;
    }

  }

}

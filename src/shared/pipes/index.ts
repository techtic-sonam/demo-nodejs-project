import { Slug } from "./slug.pipe";
import { CapitalizePipe } from "./capitalize.pipe";

export { Slug } from "./slug.pipe";
export { CapitalizePipe } from "./capitalize.pipe";

const Pipes = [
    Slug,
    CapitalizePipe
];

export { Pipes };
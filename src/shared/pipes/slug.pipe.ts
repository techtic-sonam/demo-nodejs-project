import { PipeTransform, Injectable } from '@nestjs/common';

@Injectable()
export class Slug implements PipeTransform {
    transform(value: any, operator: any = '-') {
        return value.toString().toLowerCase()
            .replace(/-/g, ' ')                     // Replace multiple dash with spaces
            .replace(/\s+/g, operator)          // Replace spaces with - or _
            .replace(/[^\w\-]+/g, '')               // Remove all non-word chars
            .replace(/\-\-+/g, operator)        // Replace multiple - with single -
            .replace(/^-+/, '')                 // Trim - from start of text
            .replace(/-+$/, '');                // Tr
    }
}
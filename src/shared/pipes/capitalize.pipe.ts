import { Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class CapitalizePipe implements PipeTransform {
	transform(value: any, operator: any = '-') {
		var frags = value.split(operator);
		frags = frags.map((item: any) => {
			return item.charAt(0).toUpperCase() + item.substring(1).toLowerCase();
		});

		return frags.join(' ');
    }
}

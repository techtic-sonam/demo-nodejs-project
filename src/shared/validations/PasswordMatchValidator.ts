import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from "class-validator";

/**
* Password and confirm password match validator
*/
@ValidatorConstraint()
export class PasswordMatch implements ValidatorConstraintInterface {

    validate(confirm_password: string, args: ValidationArguments) {
        let password = args.object['password'];
        if (password !== confirm_password) {
            return false;
        }

        return true;
    }

    defaultMessage(args: ValidationArguments) { // here you can provide default error message if validation failed
        return "The password and confirmation password field do not match.";
    }
}
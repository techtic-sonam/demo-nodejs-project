import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from "class-validator";
import { User } from "../../modules/user/user.entity";
import { getRepository } from "typeorm";
/**
* Email match validator
*/
@ValidatorConstraint()
export class EmailMatch implements ValidatorConstraintInterface {

    async validate(email: string, args: ValidationArguments) {
        const userRepository = getRepository(User);
        const user = await userRepository.findOne({ where: {email: email} });
        if (user) { 
            return true;
        };
        return false;
    }

    // here you can provide default error message if validation failed
    defaultMessage(args: ValidationArguments) {
        return "There wasn't an account for that email.";
    }
}

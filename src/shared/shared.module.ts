import { Module, DynamicModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Pipes } from './pipes';
import { Services } from './services';
import AWS = require('aws-sdk');
import { User } from '../modules/user/user.entity';
import { Dropdown } from '../modules/user/dropdown.entity';
import { Accounts } from '../modules/account/account.entity';
import { Performance } from '../modules/performance/performance.entity';
import { UserProduct } from '../modules/userproduct/userproduct.entity';
import { UserAccount } from '../modules/useraccount/useraccount.entity';
import { UserAccountPerformance } from '../modules/useraccount/useraccountsperformance.entity';
import { UserActivity } from '../modules/user/useractivity.entity';
import { UserProductAsset } from '../modules/userproductasset/userproductasset.entity';


const Entity = [
	User,
	Dropdown,
	UserProduct,
	Accounts,
	Performance,
	UserAccount,
	UserAccountPerformance,
	UserActivity,
	UserProductAsset
];

@Module({
	imports: [
		TypeOrmModule.forFeature(Entity)
	],
	exports: [
		...Pipes,
		...Services,
		TypeOrmModule.forFeature(Entity)
	],
	providers: [
		...Pipes,
		...Services,
	]
})
export class SharedModule {
	static forRoot(): DynamicModule {
		return {
			module: SharedModule,
			providers: [
				...Services,
				...Pipes
			]
		};
	}
}

import { ConfigService } from '@nestjs/config';
import _ from 'underscore';
import * as fs from 'fs';
import * as AWS from 'aws-sdk';
import { Brackets } from 'typeorm';

const configService = new ConfigService();

AWS.config.update({
    accessKeyId: configService.get('AWS_S3_ACCESS_KEY_ID'),
    secretAccessKey: configService.get('AWS_S3_SECRET_ACCESS_KEY'),
});
const s3 = new AWS.S3();

export function baseUrl(path?: string) {
    let app_url = configService.get('APP_URL');
    if (path) {
        app_url += `/${path}`;
    }
    return app_url;
}

export function assetUrl(path?: string) {
    if (path.indexOf("public") > -1) {
        path = path.replace(/\\/g, "/").replace('public/', "");
    }

    if (fs.existsSync('public/' + path)) {
        return baseUrl(path);
    }

    return baseUrl('no-img.png');
}

export function imageUrl(path?: string) {
    let APP_URL = configService.get('APP_URL');

    if (fs.existsSync('public/' + path)) {
        return baseUrl(path);
    }

    return baseUrl('no-img.png');
}

export function noImage() {
    let APP_URL = configService.get('APP_URL');
    return APP_URL + '/no-img.png';
}

export const readfile = (filepath: string, opts = 'utf8'): Promise<any> => {
    return new Promise((resolve, reject) => {
        fs.readFile(filepath, opts, (err, data) => {
            if (err) reject(err)
            else resolve(data)
        })
    })
}

export const readfileSync = (filepath: string, opts = 'utf8'): Promise<any> => {
    return new Promise((resolve, reject) => {
        var buffer = fs.readFileSync(filepath);
        resolve(buffer);
    })
}


export const titlecase = (str) => {
    return str.replace(/(?:^|\s)\w/g, function(match) {
        return match.toUpperCase();
    });
}

export const camelToSnake = (string, opts = '_') => {
    // return string.replace(/[\w]([A-Z])/g, function (m) {
    //     return m[0] + "_" + m[1];
    // }).toLowerCase();
    return string.split(/(?=[A-Z])/).join(opts).toLowerCase();
}

export const validateEmail = (string) => {
    var re = /\S+@\S+\.\S+/;
    return re.test(string);
}

export function getS3BucketUrl(image_url)  {
    if(image_url && !image_url.includes("http")) {
    const url = s3.getSignedUrl('getObject', {
        Bucket: configService.get('AWS_S3_BUCKET_NAME'),
        Key: image_url,
        Expires: 3600 * 2
    });
    image_url = url;
}
return image_url;
    }

export const validateNumber = (string) => {
    var re = /^\d+$/;
    return re.test(string);
}

export async function bindDataTableQuery(input: any, query: any = {}) {
    query.where = query.where || [];
    let tablePath = query.expressionMap.aliases[0].name;

    if (input.filter) {

        if (input.filter_in) {
            query.andWhere(new Brackets((qb: any) => {
                for (let index = 0; index < input.filter_in.length; index++) {
                    const filter = input.filter_in[index];

                    switch (filter.type) {
                        case "int":
                            let inputFilter = parseFloat(input.filter.replace(/[^0-9.-]+/g, ""));
                            if (Number.isInteger(inputFilter)) {
                                qb.orWhere(`${filter.name} like '%${inputFilter}%'`)
                            }
                            break;
                        default:
                            qb.orWhere(`${filter.name} like '%${input.filter}%'`)
                            break;
                    }
                }
            }))
        }
    }

    if (input.order) {
        query.orderBy(input.order.name, input.order.direction == 'asc' ? 'ASC' : 'DESC')
    }
    return query;
}

export async function notificationMessage(input: any) {
    let msg = null;
    switch (input.notification_id) {
        case 1:
            msg = "You have a new connection request from " + input.from_user.first_name + ' ' + input.from_user.last_name + '.';
            break;
        case 2:
            // msg = input.from_user.first_name + ' ' + input.from_user.last_name + " has accepted your connection request.";
            msg = input.from_user.first_name + ' ' + input.from_user.last_name + " is now connected with you as your "+input.user_connection.relationship_type+".";
            break;
        case 3:
            msg = "You have a new document request from " + input.from_user.first_name + ' ' + input.from_user.last_name + '.';
            break;
        case 4:
            // msg = "You have a new connection relationship change request from " + input.from_user.first_name + ' ' + input.from_user.last_name + '.';
            msg = input.from_user.first_name + ' ' + input.from_user.last_name + " has updated their relationship with you to "+input.user_connection.relationship_type+".";
            break;
        case 5:
            msg = input.from_user.first_name + ' ' + input.from_user.last_name + " has accepted your connection relationship change request.";
            break;
        case 6:
            msg = input.from_user.first_name + ' ' + input.from_user.last_name + " has requested your permission.";
            break;
        case 7:
            msg = input.from_user.first_name + ' ' + input.from_user.last_name + " has accpted your permission request.";
            break;
        case 8:
            msg = " You have a new permission relation type request : " + input.from_user.first_name + ' ' + input.from_user.last_name + ".";
            break;
        case 9:
            msg = input.from_user.first_name + ' ' + input.from_user.last_name + " has accepted your permission request for relationship change.";
            break;
        case 10:
            msg = input.from_user.first_name + ' ' + input.from_user.last_name + " declined your permission request.";
            break;
        default:
            msg = input;
            break;
    }

    return msg;

}

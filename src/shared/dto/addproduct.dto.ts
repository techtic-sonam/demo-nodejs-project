import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty} from 'class-validator';

export class AddProductDTO {
    @ApiProperty()
	@IsNotEmpty()
	name: string;

}
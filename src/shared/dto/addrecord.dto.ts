import { ApiProperty } from '@nestjs/swagger';
export class AddRecordDto {
    @ApiProperty({
        required: true,
    })
    name: string;

    @ApiProperty({
        required: true,
    })
    expirationDate: Date;

    @ApiProperty({
        required: true,
    })
    doesNotExpire: boolean;

}
import { ApiProperty } from '@nestjs/swagger';
import { integer } from 'aws-sdk/clients/cloudfront';
import { IsNotEmpty} from 'class-validator';

export class AddAssetDTO {
    @ApiProperty()
	@IsNotEmpty()
	name: string;

    @ApiProperty()
	@IsNotEmpty()
	period: integer;

}
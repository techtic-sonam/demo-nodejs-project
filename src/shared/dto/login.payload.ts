import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, MinLength } from 'class-validator';

export class LoginPayload {
	@ApiProperty()
	@IsNotEmpty()
	email: string;

	@ApiProperty()
	@IsNotEmpty()
	@MinLength(5)
	password: string;
}

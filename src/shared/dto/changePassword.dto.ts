import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, MinLength, Validate } from 'class-validator';
import { PasswordMatch } from "../../shared/validations/PasswordMatchValidator";

export class ChangePasswordDTO {

    @ApiProperty()
    @IsNotEmpty({
        message : "Current Password should not be empty"
    })
    @MinLength(8)
    old_password: string;

    @ApiProperty()
    @IsNotEmpty({
        message : "Password should not be empty"
    })
    @MinLength(8)
    password: string;

    @ApiProperty()
    @IsNotEmpty({
        message : "The confirm password should not be empty"
    })
    //validate password and confirm password match
    @Validate(PasswordMatch, {
        message : "The confirmation password field does not match with password field."
    })
    confirm_password: string;
}

import { ApiProperty } from "@nestjs/swagger";
// import { IsNotEmpty, Validate } from "class-validator";
/* import { PasswordMatch } from "../../shared/validations/PasswordMatchValidator"; */


export class ResetPasswordDTO {
    /**
    * Password parameter
    */
    @ApiProperty()
    // @IsNotEmpty({
    //     message : "The password should not be empty"
    // })
    password: string;

    /**
    * Confirm password parameter
    */
    @ApiProperty()
    // @IsNotEmpty({
    //     message : "The confirm password should not be empty"
    // })
    //validate password and confirm password match
    // @Validate(PasswordMatch, {
    //     message : "The password and confirmation password field do not match."
    // })
    password_confirmation: string;
}
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, MinLength, Validate } from 'class-validator';

export class RegisterDTO {
    @ApiProperty()
	@IsNotEmpty()
	first_name: string;

    @ApiProperty()
	@IsNotEmpty()
	last_name: string;

    // @ApiProperty()
	// @IsNotEmpty()
	// role_id: number;

    @ApiProperty()
	@IsNotEmpty()
	email: string;

}
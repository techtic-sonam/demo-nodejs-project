import * as crypto from 'crypto';


export function becrypt(password: string) {
    return crypto.createHmac('sha256', password).digest('hex');
}

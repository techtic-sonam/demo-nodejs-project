import { Module, DynamicModule } from '@nestjs/common';
import { SharedModule } from '../../shared/shared.module';
import { UserProductAssetController } from './userproductasset.controller';


@Module({
	controllers: [UserProductAssetController],
	imports: [
		SharedModule
	]
})
export class UserProductAssetModule {
	static forRoot(): DynamicModule {
		return {
		  	module: UserProductAssetModule,
		};
	}
}
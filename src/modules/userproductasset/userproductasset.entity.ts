import { Entity, PrimaryGeneratedColumn, Column} from 'typeorm';

@Entity('user_products_assets')
export class UserProductAsset {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    user_id: number;

    @Column()
    name: string;

    @Column()
    period:number;

    @Column()
    USCoreFixedIncome: string;

    @Column()
    USEquity: string;

    @Column()
    EAFEEquity: string;

    @Column()
    RealAssets: string;

    @Column()
    CashEquivalents: string;

    @Column()
    EquityHedgeFunds: string;

    @Column()
    GlobalREITs: string;

    @Column()
    GlobalCoreFixedIncome: string;

    @Column()
    BankLoans: string;

    @Column()
    USHighYieldFixedIncome: string;

    @Column()
    GlobalHighYieldFixedIncome: string;

    @Column()
    EmergingMarketsDebt: string;

    @Column()
    EmergingMarketEquity: string;

    @Column()
    MunicipalFixedIncome: string;

    @Column()
    AbsoluteReturn: string;

    @Column()
    USREITs: string;

    @Column()
    CreditHedgeFunds: string;

    @Column()
    MacroHedgeFunds: string;

    @Column()
    PrivateEquity: string;

    @Column()
    VentureCapital: string;

    @Column()
    PrivateDebt: string;

    @Column()
    PrivateRealEstate: string;

    @Column()
    Commodities: string;

    @Column()
    GlobalEquity: string;

    @Column()
    Treasuries: string;



}
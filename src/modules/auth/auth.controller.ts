import { Controller, Body, Post, Get, UseGuards, Request, Res, HttpStatus, NotFoundException, Inject, forwardRef, BadRequestException } from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiOkResponse, ApiBadRequestResponse, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { UsersService } from '../../shared/services';
import { AuthService } from '.';
import { LoginPayload } from '../../shared/dto/login.payload';
import * as _ from 'underscore';

@Controller('auth')
@ApiTags('Authentication')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
  ) {}


  @Post('login')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async login(
    @Body() payload: LoginPayload,
    @Res() res: Response,
  ): Promise<any> {
     return await this.authService.validateUser(payload).then(async (user) => {
      if (!user) {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
          message: 'User Not Found',
        });
      } else {
        const token = this.authService.createToken(user);
        //user = await this.getAllInfoUser(user);
        return res.status(HttpStatus.OK).json({
          status: HttpStatus.OK,
          data: Object.assign(token, { user: user }),
        });
      }
    });
  }

  @ApiBearerAuth()
  @Post('logout')
  @UseGuards(AuthGuard('jwt'))
  @ApiOkResponse({ description: 'Successful response' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async logout(@Request() request: any, @Res() res: Response): Promise<any> {
    try {
      return await this.authService.logout(request).then(() => {
        return res.status(HttpStatus.OK).json({
          status: 200,
          message: 'Successfully logged out',
        });
      });
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  @ApiBearerAuth()
  @Post('refresh-token')
  @ApiOkResponse({ description: 'Successful response' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async refreshToken(@Body() payload: any, @Res() res: Response): Promise<any> {
    if (!payload.token) {
      throw new NotFoundException('No token provided.');
    }

    let data = await this.authService.refreshToken(payload);
    return res.status(HttpStatus.OK).json({
      status: HttpStatus.OK,
      data: {
        token: data.token,
        user: data.user,
      },
    });
  }

}

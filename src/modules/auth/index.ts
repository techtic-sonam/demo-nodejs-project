import { createParamDecorator, UnauthorizedException } from '@nestjs/common';

export * from './auth.service';
export * from './jwt.strategy';
export * from './auth.module';
export * from './auth.controller';
export * from './jwt-guard';


export const authenticated = next => (root, args, context, info) => {
  if (!context.currentUser) {
    throw new Error(`Unauthenticated!`);
  }

  return next(root, args, context, info);
};

export const CurrentUser = createParamDecorator(
  (data, [root, args, ctx, info]) => {
    return ctx.req.user
},
);

export const JwtToken = createParamDecorator(
  (data, [root, args, ctx, info]) => ctx.jwtToken,
);

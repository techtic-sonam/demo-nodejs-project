import { Injectable, UnauthorizedException, NotAcceptableException, Inject, forwardRef, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { SignOptions } from 'jsonwebtoken';
import { UserProductAssetService, UserProductService, UsersService } from '../../shared/services';
import { User } from '../user/user.entity';
import { LoginPayload } from '../../shared/dto/login.payload';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as _ from 'underscore';
import { UserProduct } from '../userproduct/userproduct.entity';
import { UserProductAsset } from '../userproductasset/userproductasset.entity';


@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly usersProductService: UserProductService,
    @InjectRepository(User) private readonly userProductRepository: Repository<UserProduct>,
    private readonly userProductAssetService: UserProductAssetService,
    @InjectRepository(User) private readonly userProductAssetRepository: Repository<UserProductAsset>,

  ) {}

  createToken(user: User, options: SignOptions = {}) {
    const payload = { email: user.email, id: user.id };
    return {
      token: this.jwtService.sign(payload, options),
    };
  }

  async validateUser(payload: LoginPayload): Promise<any> {
    const verify = await this.usersService.getByEmailAndPass(
      payload.email,
      payload.password,
    );
    if (!verify) {
      throw new UnauthorizedException(
        'You have entered an invalid email or password.',
      );
    }

    //update last_login time
    let user = await this.usersService.loginUser({email: verify.email});
    user.last_login = new Date();
    await this.userRepository.update(user.id, user);

    //entry in user activities
    let  userActivityCount = await this.usersService.userLoginActivity(user.id);

    //delete user products and assets by user id
    this.usersProductService.deleteProductByUserId(user.id);
    this.userProductAssetService.deleteProductByUserId(user.id);

    let userData = await this.usersService.findOne({ email: verify.email }, [
        'role',
    ]);

    userData['login_count'] = userActivityCount;
    return userData;

  }

  async refreshToken(request: any): Promise<any> {
    // invalid token - synchronous
    try {
      var decoded = this.jwtService.decode(request.token, { complete: true });
      let payload = decoded['payload'];

      if (payload) {
        delete payload.iat;
        delete payload.exp;
      }

      let token = this.jwtService.sign({
        email: payload.email,
        id: payload.id,
      });
      // let user = await this.usersService.loginUser({ email: payload.email });
      let userData = await this.usersService.findOne({ email: payload.email }, [
        'role',
      ]);

      return {
        token: token,
        user: userData,
      };
    } catch (err) {
      throw new NotAcceptableException('provided token is invalid.');
    }
  }

  async logout(request: any) {
    let user = request.user;
    let timestamp = Date.now() / 100;
    return true;
  }

  async verifyToken(payload: any): Promise<any> {
    const token = "";//await this.usersService.verifyToken(payload);

    if (!token) {
      throw new NotAcceptableException(
        "Token is expired or you cant't access this request.",
      );
    }

    return token;
  }

  async demoFunc(payload): Promise<any> {
    return payload;
  }
}

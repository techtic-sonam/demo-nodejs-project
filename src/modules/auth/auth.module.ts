import { Module, DynamicModule } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { JwtStrategy } from './jwt.strategy';
import { AuthController } from './auth.controller';
import { SharedModule } from '../../shared/shared.module';
import { ConfigService } from '@nestjs/config';

@Module({
	imports: [
		SharedModule,
		PassportModule.register({ defaultStrategy: 'jwt' }),
		JwtModule.registerAsync({
			imports: [
			],
			useFactory: async (configService: ConfigService) => {
				return {
				secret: configService.get<string>('JWT_SECRET_KEY'),
				signOptions: {
					...(
					configService.get('JWT_EXPIRATION_TIME')
						? {
						expiresIn: Number(configService.get('JWT_EXPIRATION_TIME')),
						}
						: {}
					),
				},
				};
			},
			inject: [
				ConfigService,
			],
		}),
	],
	controllers: [
		AuthController,
	],
	providers: [
		AuthService,
		JwtStrategy,
	],
	exports: [
		PassportModule.register({ defaultStrategy: 'jwt' }),
	],
})
export class AuthModule { 
	static forRoot(): DynamicModule {
		return {
		  	module: AuthModule,
		};
	}
}

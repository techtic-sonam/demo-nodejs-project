import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity({
    name: 'users',
})

export class PasswordReset {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255 })
    email: string;


    @CreateDateColumn()
    public created_at: Date;

    @UpdateDateColumn()
    public updated_at: Date;
}

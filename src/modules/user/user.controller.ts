import { Controller, UnprocessableEntityException, Body, Post, Get, Delete, UseGuards, Param, Request, Res, HttpStatus, NotFoundException, Inject, forwardRef, BadRequestException } from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiOkResponse, ApiBadRequestResponse, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { UsersService } from '../../shared/services';
import { LoginPayload } from '../../shared/dto/login.payload';
import * as _ from 'underscore';
import { RegisterDTO } from '../../shared/dto/register.dto';
import { ForgotPasswordDTO } from '../../shared/dto/forgotPassword.dto';

@Controller('user')
@ApiTags('Authentication')
export class UserController {
  constructor(
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
  ) {}


  @Post('')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async updateUser(
    @Request() request: any,
    @Body() RegisterPayload: RegisterDTO,
    @Res() res: Response,
    @Request() req,
  ): Promise<any> {
    return await this.usersService
    .create(RegisterPayload)
    .then(async reasons => {
      return res.status(HttpStatus.OK).json({
        status: HttpStatus.OK,
        data: reasons,
      });
    })
    .catch((error: any) => {
      throw new UnprocessableEntityException(error);
    });
  }



  @Post('')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async profileUser(
    @Request() request: any,
    @Body() RegisterPayload: RegisterDTO,
    @Res() res: Response,
    @Request() req,
  ): Promise<any> {
    return await this.usersService
    .create(RegisterPayload)
    .then(async reasons => {
      return res.status(HttpStatus.OK).json({
        status: HttpStatus.OK,
        data: reasons,
      });
    })
    .catch((error: any) => {
      throw new UnprocessableEntityException(error);
    });
  }


  @UseGuards(AuthGuard('jwt'))
  @Get('')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async getAll(
    @Body() body: any,
    @Res() res: Response,
  ): Promise<any> {
    return await this.usersService
      .getAll()
      .then(async reasons => {
        return res.status(HttpStatus.OK).json({
          status: HttpStatus.OK,
          data: reasons,
        });
      })
      .catch((error: any) => {
        throw new UnprocessableEntityException(error);
      });
  }


  @Delete('/:id')
  async delete(
    @Param('id') id,
    @Res() res: Response
  ): Promise<any> {

    return await this.usersService
      .delete(id)
      .then(async reasons => {
        return res.status(HttpStatus.OK).json({
          status: HttpStatus.OK,
          data: 'User deleted successfully',
        });
      })
      .catch((error: any) => {
        throw new UnprocessableEntityException(error);
      });
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('licence')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async getLicence(@Request() request: any, @Res() res: Response, @Param('licence') licence = null): Promise<any> {
    let fieldType="license";
    return await this.usersService.getDropdownByParam(fieldType)
      .then(async output => {
        return res.status(HttpStatus.OK).json({
          status: HttpStatus.OK,
          data: output,
        });
      })
      .catch((error: any) => {
        throw new UnprocessableEntityException(error);
      });
  }

  @Get(':id')
  async findOne(
    @Param('id') id,
    @Res() res: Response
  ): Promise<any> {
    return await this.usersService
      .findOne({ uuid: id } , ['license'])
      .then(async data => {
        return res.status(HttpStatus.OK).json({
          status: HttpStatus.OK,
          data: data,
        });
      })
      .catch((error: any) => {
        throw new UnprocessableEntityException(error);
      });
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('change-password')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async search(
    @Request() request: any,
    @Body() body: any,
    @Res() res: Response,
    @Request() req,
  ): Promise<any> {
    let loginUser = request.user ? request.user.id : '';
    return await this.usersService
    .changePassword(loginUser,body)
    .then(async reasons => {
      return res.status(HttpStatus.OK).json({
        status: HttpStatus.OK,
        data: reasons,
      });
    })
    .catch((error: any) => {
      throw new UnprocessableEntityException(error);
    });
  }

  @Post('reset-password')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async reste(
    @Request() request: any,
    @Body() body: any,
    @Res() res: Response,
    @Request() req,
  ): Promise<any> {
    let loginUser = request.user ? request.user.id : '';
    return await this.usersService
    .resetPassword(body)
    .then(async reasons => {
      return res.status(HttpStatus.OK).json({
        status: HttpStatus.OK,
        data: reasons,
      });
    })
    .catch((error: any) => {
      throw new UnprocessableEntityException(error);
    });
  }


  @Post('forgot-password')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async forgotUserPassword(
    @Body() payload: ForgotPasswordDTO,
    @Res() response: Response,
  ): Promise<any> {
    try {
      return await this.usersService
        .sendForgotPasswordEmail(payload)
        .then(response_message => {
          return response.status(HttpStatus.OK).json({
            status: HttpStatus.OK,
            message: response_message,
          });
        })
        .catch((error: any) => {
          throw new BadRequestException(error);
        });
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  @Post('setuserpassword')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async setUserPassword(
    @Request() request: any,
    @Body() body: any,
    @Res() res: Response,
    @Request() req,
  ): Promise<any> {
    return await this.usersService
    .setUserPassword(body)
    .then(async reasons => {
      return res.status(HttpStatus.OK).json({
        status: HttpStatus.OK,
        data: reasons,
      });
    })
    .catch((error: any) => {
      throw new UnprocessableEntityException(error);
    });
  }

}

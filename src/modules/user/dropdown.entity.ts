import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, AfterLoad, OneToMany, ManyToMany, JoinTable, JoinColumn, OneToOne, ManyToOne } from 'typeorm';
import { Exclude } from 'class-transformer';
import { User } from './user.entity';

@Entity('dropdowns')
export class Dropdown {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 255 })
  field: string;

  @Column({ length: 255 })
  display_text: string;

  @Column({ length: 255 })
  display_value: string;

  @CreateDateColumn()
  public created_at: Date;

  @UpdateDateColumn()
  public updated_at: Date;

  @OneToOne(type => User, user => user.license)
  @JoinColumn({ name: "id" })
  user: User;

}

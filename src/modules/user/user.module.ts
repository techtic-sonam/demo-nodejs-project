import { Module, DynamicModule } from '@nestjs/common';
import { SharedModule } from '../../shared/shared.module';
import { UserController } from './user.controller';


@Module({
	controllers: [UserController],
	imports: [
		SharedModule
	]
})
export class UserModule {
	static forRoot(): DynamicModule {
		return {
		  	module: UserModule,
		};
	}
}
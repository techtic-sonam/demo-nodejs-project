import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, AfterLoad, OneToMany, ManyToMany, JoinTable, JoinColumn, OneToOne, ManyToOne } from 'typeorm';
import { Exclude } from 'class-transformer';
import { DateTime } from 'aws-sdk/clients/devicefarm';
import { Dropdown } from './dropdown.entity';
import { Role } from '../role/role.entity';


@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  uuid: string;

  @Column({ length: 255 })
  email: string;

  @Exclude()
  @Column({
    select: false,
    name: 'password',
    length: 255,
  })
  password: string;

  @Column({ length: 255 })
  phone_no: string;

  @Column({ length: 255 })
  first_name: string;

  @Column({ length: 255 })
  last_name: string;

  @Column({
    select: true,
    name: 'profile_image_url',
    length: 255,
  })
  profile_image_url: string;

  @Column()
  company: string;


  @Column()
  last_login: Date;

  // @Column()
  // login_count: number;

  @OneToOne(type => Dropdown, license => license.user)
  @JoinColumn({ name: "license" })
  license: Dropdown;

  @Column()
  role_id: number;

  @OneToOne(type => Role)
  @JoinColumn({ name: 'role_id', referencedColumnName: 'id' })
  role: Role;


}

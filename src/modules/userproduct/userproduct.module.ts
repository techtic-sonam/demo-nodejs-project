import { Module, DynamicModule } from '@nestjs/common';
import { SharedModule } from '../../shared/shared.module';
import { UserProductController } from './userproduct.controller';


@Module({
	controllers: [UserProductController],
	imports: [
		SharedModule
	]
})
export class UserProductModule {
	static forRoot(): DynamicModule {
		return {
		  	module: UserProductModule,
		};
	}
}
import { Controller, UnprocessableEntityException, Body, Post, Get, Delete, UseGuards, Param, Request, Res, HttpStatus, NotFoundException, Inject, forwardRef, BadRequestException } from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiOkResponse, ApiBadRequestResponse, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { UserProductService } from '../../shared/services/userproduct.service';
import * as _ from 'underscore';
import { AddProductDTO } from '../../shared/dto/addproduct.dto';

@Controller('userproduct')
@ApiTags('Authentication')
export class UserProductController {
  constructor(
    @Inject(forwardRef(() => UserProductService))
    private readonly userProductService: UserProductService,
  ) {}

  @UseGuards(AuthGuard('jwt'))
  @Post('')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async updateUserProduct(
    @Request() request: any,
    @Body() body: AddProductDTO,
    @Res() res: Response,
    @Request() req,
  ): Promise<any> {
    let loginUser = request.user ? request.user.id : '';
    return await this.userProductService
    .create(body,loginUser)
    .then(async reasons => {
      return res.status(HttpStatus.OK).json({
        status: HttpStatus.OK,
        data: reasons,
      });
    })
    .catch((error: any) => {
      throw new UnprocessableEntityException(error);
    });
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async getAll(
    @Body() body: any,
    @Res() res: Response,
  ): Promise<any> {
    return await this.userProductService
      .getAll()
      .then(async reasons => {
        return res.status(HttpStatus.OK).json({
          status: HttpStatus.OK,
          data: reasons,
        });
      })
      .catch((error: any) => {
        throw new UnprocessableEntityException(error);
      });
  }

  @Get(':id')
  async findOne(
    @Param('id') id,
    @Res() res: Response
  ): Promise<any> {
    return await this.userProductService
      .findOne({ id: id })
      .then(async data => {
        return res.status(HttpStatus.OK).json({
          status: HttpStatus.OK,
          data: data,
        });
      })
      .catch((error: any) => {
        throw new UnprocessableEntityException(error);
      });
  }

  @Delete('/:id')
  async delete(
    @Param('id') id,
    @Res() res: Response
  ): Promise<any> {

    return await this.userProductService
      .delete(id)
      .then(async reasons => {
        return res.status(HttpStatus.OK).json({
          status: HttpStatus.OK,
          data: 'Product deleted successfully',
        });
      })
      .catch((error: any) => {
        throw new UnprocessableEntityException(error);
      });
  }


}
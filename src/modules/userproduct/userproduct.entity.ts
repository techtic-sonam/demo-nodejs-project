import { Entity, PrimaryGeneratedColumn, Column} from 'typeorm';

@Entity('user_products')
export class UserProduct {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    user_id: number;

    @Column()
    name: string;

    @Column()
    period:number;

    @Column()
    three_month: string;

    @Column()
    ytd: string;

    @Column()
    one_year: string;

    @Column()
    three_year: string;

    @Column()
    five_year: string;

    @Column()
    seven_year: string;

    @Column()
    ten_year: string;

}
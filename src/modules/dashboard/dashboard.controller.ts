import { Controller, UnprocessableEntityException, Body, Post, Get, Delete, UseGuards, Param, Request, Res, HttpStatus, NotFoundException, Inject, forwardRef, BadRequestException } from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiOkResponse, ApiBadRequestResponse, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { DashboardService } from '../../shared/services/dashboard.service';
import { UsersService } from '../../shared/services/users.service';


@Controller('')
@ApiTags('Authentication')
export class DashboardController {
  constructor(
    @Inject(forwardRef(() => DashboardService))
    private readonly dashboardService: DashboardService,
    @Inject(forwardRef(() => UsersService))
    private readonly userService: UsersService,
  ) {}

  @UseGuards(AuthGuard('jwt'))
  @Post('search')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async search(
    @Request() request: any,
    @Body() body: any,
    @Res() res: Response,
    @Request() req,
  ): Promise<any> {
    let loginUser = request.user ? request.user.id : '';
    return await this.dashboardService
    .search(body,loginUser)
    .then(async reasons => {
      return res.status(HttpStatus.OK).json({
        status: HttpStatus.OK,
        data: reasons,
      });
    })
    .catch((error: any) => {
      throw new UnprocessableEntityException(error);
    });
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('dropdowns/account-type')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async getAccountType(
    @Body() body: any,
    @Res() res: Response,
  ): Promise<any> {
    let fieldType="account type";
    return await this.userService
      .getDropdownByParam(fieldType)
      .then(async reasons => {
        return res.status(HttpStatus.OK).json({
          status: HttpStatus.OK,
          data: reasons,
        });
      })
      .catch((error: any) => {
        throw new UnprocessableEntityException(error);
      });
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('dropdowns/account-size')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async getAccountSize(
    @Body() body: any,
    @Res() res: Response,
  ): Promise<any> {
    let fieldType="account size";
    return await this.userService
      .getDropdownByParam(fieldType)
      .then(async reasons => {
        return res.status(HttpStatus.OK).json({
          status: HttpStatus.OK,
          data: reasons,
        });
      })
      .catch((error: any) => {
        throw new UnprocessableEntityException(error);
      });
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('dropdowns/risks')
  @ApiOkResponse({ description: 'Successfully authenticated' })
  @ApiBadRequestResponse({ description: 'Bad Request' })
  async getRisksData(
    @Body() body: any,
    @Res() res: Response,
  ): Promise<any> {
    let fieldType="risk";
    return await this.userService
      .getDropdownByParam(fieldType)
      .then(async reasons => {
        return res.status(HttpStatus.OK).json({
          status: HttpStatus.OK,
          data: reasons,
        });
      })
      .catch((error: any) => {
        throw new UnprocessableEntityException(error);
      });
  }

}
import { Module, DynamicModule } from '@nestjs/common';
import { SharedModule } from '../../shared/shared.module';
import { DashboardController } from './dashboard.controller';


@Module({
	controllers: [DashboardController],
	imports: [
		SharedModule
	]
})
export class DashboardModule {
	static forRoot(): DynamicModule {
		return {
		  	module: DashboardModule,
		};
	}
}